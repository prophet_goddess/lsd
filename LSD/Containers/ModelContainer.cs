﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using MoonTools.Core.Graph;
using Smuggler;

namespace LSD.Containers
{
    public static class ModelContainer
    {

        public static Dictionary<string, Smuggler.Model> models;

        public static void LoadModels(string directory, GraphicsDevice graphicsDevice)
        {
            var files = Directory.GetFiles(directory, "*.glb", SearchOption.AllDirectories);
            models = new Dictionary<string, Smuggler.Model>();

            foreach (var file in files)
            {

                Debug.WriteLine("Loading model in file " + file);
                using(var stream = File.OpenRead(file))
                {
                    var key = Path.GetFileNameWithoutExtension(file);
                    
                    var model = Importer.ImportGLB(graphicsDevice, stream);

                    models.Add(key, model);
                }
            }
        }
    }
}
