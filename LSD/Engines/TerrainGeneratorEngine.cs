﻿using BulletSharp;
using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using LSD.Components;
using LSD.Messages;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SharpGLTF.Schema2;
using System.Diagnostics;
using System.Linq;

namespace LSD.Engines
{
    [Reads(typeof(TerrainComponent))]
    [Writes(typeof(TerrainComponent))]
    [Sends(typeof(SetTransformMessage), typeof(SetModelMessage), typeof(CreateRigidbodyMessage))]
    [Receives(typeof(GenerateTerrainMessage))]
    public class TerrainGeneratorEngine : Engine
    {
        private GraphicsDevice graphicsDevice;

        public TerrainGeneratorEngine(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
        }

        public override void Update(double dt)
        {
            foreach (var generateTerrain in ReadMessages<GenerateTerrainMessage>())
            {
                var entity = CreateEntity();

                SetComponent(entity, new TerrainComponent());

                SetTransformMessage setTransform;
                setTransform.entity = entity;
                setTransform.position = new Vector3(generateTerrain.position.X, generateTerrain.position.Y, 0f);
                setTransform.orientation = Quaternion.Identity;
                SendMessage(setTransform);

                Vector3[] positions = new Vector3[(generateTerrain.size + 1) * (generateTerrain.size + 1)];
                VertexPositionColorTexture[] vertices = new VertexPositionColorTexture[positions.Length];

                for (int i = 0, x = -generateTerrain.size / 2; x <= generateTerrain.size / 2; x++)
                {
                    for (int y = -generateTerrain.size / 2; y <= generateTerrain.size / 2; y++, i++)
                    {
                        positions[i] = new Vector3(x, y, RNG.GetFloat(-1f, 1f));

                        var color = new Color((x + generateTerrain.size / 2f) / generateTerrain.size, (y + generateTerrain.size / 2f) / generateTerrain.size, 0f);

                        vertices[i] = new VertexPositionColorTexture(positions[i], color, Vector2.Zero);
                    }
                }

                int[] indices = new int[generateTerrain.size * generateTerrain.size * 6];
                Smuggler.Triangle[] triangles = new Smuggler.Triangle[indices.Length/3];

                for (int index = 0, vertIndex = 0, x = 0; x < generateTerrain.size; x++, vertIndex++)
                {
                    for (int y = 0; y < generateTerrain.size; y++, index += 6, vertIndex++)
                    {
                        indices[index + 2] = vertIndex;
                        indices[index + 5] = indices[index] = vertIndex + 1;
                        indices[index + 4] = indices[index + 1] = vertIndex + generateTerrain.size + 1;
                        indices[index + 3] = vertIndex + generateTerrain.size + 2;

                        triangles[vertIndex] = new Smuggler.Triangle(indices[index], indices[index + 1], indices[index + 2]);
                        triangles[vertIndex + 1] = new Smuggler.Triangle(indices[index + 3], indices[index + 4], indices[index + 5]);
                    }
                }

                VertexBuffer vertexBuffer = new VertexBuffer(graphicsDevice, typeof(VertexPositionColorTexture), positions.Length, BufferUsage.WriteOnly);
                vertexBuffer.SetData(vertices);

                IndexBuffer indexBuffer = new IndexBuffer(graphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Length, BufferUsage.WriteOnly);
                indexBuffer.SetData(indices);

                var effect = new BasicEffect(graphicsDevice);
                effect.VertexColorEnabled = true;

                Smuggler.MeshPart meshPart = new Smuggler.MeshPart(vertexBuffer, indexBuffer, positions, triangles, effect);

                Smuggler.Mesh mesh = new Smuggler.Mesh(new Smuggler.MeshPart[] { meshPart });

                Smuggler.Model model = new Smuggler.Model(new Smuggler.Mesh[] { mesh });

                SetModelMessage setModel;
                setModel.entity = entity;
                setModel.model = model;
                SendMessage(setModel);

                CreateRigidbodyMessage createRigidbodyMessage;
                createRigidbodyMessage.angularFactor = Vector3.Zero;
                createRigidbodyMessage.entity = entity;
                createRigidbodyMessage.linearFactor = Vector3.Zero;
                createRigidbodyMessage.orientation = setTransform.orientation;
                createRigidbodyMessage.position = setTransform.position;
                createRigidbodyMessage.constructionInfo = new RigidBodyConstructionInfo(0f, null, ShapeGenerator.CreateShapeFromGlTFModel(model));
                SendMessage(createRigidbodyMessage);

            }
        }
    }
}
