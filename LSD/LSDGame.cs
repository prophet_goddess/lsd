using Encompass;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BulletSharp;
using Encompass3D;
using Encompass3D.Engines;
using Encompass3D.Renderers;
using Encompass3D.Components;
using Encompass3D.Messages;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Encompass3D.Utility;
using LSD.Containers;
using System.Diagnostics;
using LSD.Engines;
using LSD.Messages;

namespace LSD
{
    class LSDGame : Game
    {
        GraphicsDeviceManager graphics;

        WorldBuilder WorldBuilder { get; } = new WorldBuilder();
        World World { get; set; }

        public LSDGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
            IsFixedTimeStep = true;
            IsMouseVisible = false;
            graphics.SynchronizeWithVerticalRetrace = true;

            Window.AllowUserResizing = true;
            IsMouseVisible = false;

        }

        protected override void LoadContent()
        {

            ModelContainer.LoadModels(Content.RootDirectory, GraphicsDevice);
            RNG.Initialize();

            WorldBuilder.AddEngine(new PhysicsEngine());
            WorldBuilder.AddEngine(new TransformEngine());
            WorldBuilder.AddEngine(new ModelEngine());
            WorldBuilder.AddEngine(new CameraEngine());
            WorldBuilder.AddEngine(new CharacterControllerEngine());
            WorldBuilder.AddEngine(new InputEngine(this));
            WorldBuilder.AddEngine(new TerrainGeneratorEngine(GraphicsDevice));

            WorldBuilder.AddGeneralRenderer(new ModelRenderer(GraphicsDevice, graphics), 0);

            var collisionConf = new DefaultCollisionConfiguration();
            var dispatcher = new CollisionDispatcher(collisionConf);
            var broadphase = new DbvtBroadphase();
            var constraintSolver = new SequentialImpulseConstraintSolver();

            var physicsEntity = WorldBuilder.CreateEntity();
            PhysicsComponent physicsComponent;
            var gravity = new BulletSharp.Math.Vector3(0f, 0f, -9.8f);
            physicsComponent.world = new DiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConf);
            physicsComponent.world.SetGravity(ref gravity);

            WorldBuilder.SetComponent(physicsEntity, physicsComponent);

            var inputEntity = WorldBuilder.CreateEntity();
            InputComponent inputComponent;
            inputComponent.inputs = new Dictionary<string, Input>
            {
                ["forwards"] = new Input(new Keys[] { Keys.W }, new Buttons[] { Buttons.LeftThumbstickUp }),
                ["backwards"] = new Input(new Keys[] { Keys.S }, new Buttons[] { Buttons.LeftThumbstickDown }),
                ["left"] = new Input(new Keys[] { Keys.A }, new Buttons[] { Buttons.LeftThumbstickLeft }),
                ["right"] = new Input(new Keys[] { Keys.D }, new Buttons[] { Buttons.LeftThumbstickRight }),
                ["jump"] = new Input(new Keys[] { Keys.Space }, new Buttons[] { Buttons.A }),
            };

            WorldBuilder.SetComponent(inputEntity, inputComponent);

            GenerateTerrainMessage generateTerrain;
            generateTerrain.position = Vector2.Zero;
            generateTerrain.size = 50;
            WorldBuilder.SendMessage(generateTerrain);

            for (int i = 0; i < 25; i++)
            {
                Entity testObject = WorldBuilder.CreateEntity();

                SetTransformMessage testTransform;
                testTransform.entity = testObject;
                testTransform.orientation = Quaternion.Identity;
                testTransform.position = new Vector3(RNG.GetFloat(), RNG.GetFloat(), 5f + RNG.GetFloat());
                WorldBuilder.SendMessage(testTransform);

                SetModelMessage testModel;
                testModel.entity = testObject;
                testModel.model = ModelContainer.models["cube"];
                WorldBuilder.SendMessage(testModel);

                CreateRigidbodyMessage testRigidbody;
                testRigidbody.entity = testObject;
                testRigidbody.orientation = testTransform.orientation;
                testRigidbody.position = testTransform.position;
                testRigidbody.linearFactor = Vector3.One;
                testRigidbody.angularFactor = Vector3.One;
                testRigidbody.constructionInfo = new RigidBodyConstructionInfo(1f, null, new BoxShape(1f));
                testRigidbody.constructionInfo.LocalInertia = testRigidbody.constructionInfo.CollisionShape.CalculateLocalInertia(testRigidbody.constructionInfo.Mass);
                WorldBuilder.SendMessage(testRigidbody);

            }

            Entity testCamera = WorldBuilder.CreateEntity();

            SetTransformMessage setCameraTransform;
            setCameraTransform.entity = testCamera;
            setCameraTransform.orientation = Quaternion.Identity;
            setCameraTransform.position = new Vector3(0f, -8f, 2f);
            WorldBuilder.SendMessage(setCameraTransform);

            SetCameraMessage setCamera;
            setCamera.entity = testCamera;
            setCamera.farPlane = 200;
            setCamera.nearPlane = 0.1f;
            setCamera.fieldOfView = MathHelper.PiOver4;
            setCamera.verticalTilt = 0f;
            setCamera.offset = new Vector3(0f, 0f, 1f);
            WorldBuilder.SendMessage(setCamera);

            CreateRigidbodyMessage createCameraRigidbody;
            createCameraRigidbody.position = setCameraTransform.position;
            createCameraRigidbody.orientation = setCameraTransform.orientation;
            createCameraRigidbody.entity = testCamera;
            createCameraRigidbody.constructionInfo = new RigidBodyConstructionInfo(1f, null, new CapsuleShapeZ(0.5f, 2));
            createCameraRigidbody.linearFactor = Vector3.One;
            createCameraRigidbody.angularFactor = Vector3.Zero;
            WorldBuilder.SendMessage(createCameraRigidbody);

            CharacterControllerComponent firstPersonComponent;
            firstPersonComponent.lookSensitivity = 0.1f;
            firstPersonComponent.speed = 50f;
            firstPersonComponent.jumpForce = 5f;
            firstPersonComponent.maxJumps = 1;
            firstPersonComponent.numJumps = 0;

            WorldBuilder.SetComponent(testCamera, firstPersonComponent);

            World = WorldBuilder.Build();

        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {

            World.Update(gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {

            //graphics.GraphicsDevice.RasterizerState.CullMode = CullMode.None;

            GraphicsDevice.Clear(Color.CornflowerBlue);
            World.Draw();

            base.Draw(gameTime);
        }
    }
}
