﻿using Encompass;
using Microsoft.Xna.Framework;

namespace LSD.Messages
{
    public struct GenerateTerrainMessage : IMessage
    {
        public int size;
        public Vector2 position;
    }
}
